package Environment;

import Core.Factory;
import Gameplay.CharacterController;
import Gameplay.PCamera;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioRenderer;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.InputManager;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;

public class Scene {

    public static Factory factory;
    private Setting setting;
    private CharacterController character;
    private PCamera pCamera;
    private final Vector3f SCENE_GRAVITY = new Vector3f(0f, -40f, 0f);

    public enum Setting {
        GaizerValley
    }

    public Scene(Setting setting) {
        this.setting = setting;
    }

    public void Init(Camera c,
            InputManager iM,
            AssetManager aM,
            Node rN,
            AudioRenderer aR,
            BulletAppState pH) {
        //Setup our factory
        factory = new Factory(c, aM, iM, rN, aR, pH);
        //Setup our own camera
        pCamera = new PCamera(c);
        pCamera.setEnabled(true);
        pCamera.registerWithInput(iM);
        //Initialise scene elements.
        InitialiseTerrain();
        InitialiseCharacter(c, iM);
    }

    public void InitialiseTerrain() {
        //Switch available terrain enums and init the relative one.
        switch (setting) {
            case GaizerValley:
                factory.CreateTerrain();
                break;
        }
    }

    public void InitialiseCharacter(Camera c, InputManager iM) {
        //Setup character attributes.
        float radius = 1.5f;
        float height = 6f;
        float mass = 3f;
        Vector3f gravity = new Vector3f(SCENE_GRAVITY);
        //Initialise character and call spawn.
        character = new CharacterController(c, radius, height, mass, gravity, iM);
        character.SpawnCharacter();
    }

}
