package Gameplay;

import com.jme3.input.FlyByCamera;
import com.jme3.renderer.Camera;

public class PCamera extends FlyByCamera {

    public PCamera(Camera cam) {
        super(cam);
    }

    @Override
    public void onAnalog(String name, float value, float tpf) {
        if (!enabled) {
            return;
        }

        if (name.equals("FLYCAM_Left")) {
            rotateCamera(value, initialUpVec);
        } else if (name.equals("FLYCAM_Right")) {
            rotateCamera(-value, initialUpVec);
        } else if (name.equals("FLYCAM_Up")) {
            if (cam.getDirection().y < 0.9) {
                rotateCamera(-value * (invertY ? -1 : 1), cam.getLeft());
            }
        } else if (name.equals("FLYCAM_Down")) {
            if (cam.getDirection().y > -0.9) {
                rotateCamera(value * (invertY ? -1 : 1), cam.getLeft());
            }
        } else if (name.equals("FLYCAM_Forward")) {
            moveCamera(value, false);
        } else if (name.equals("FLYCAM_Backward")) {
            moveCamera(-value, false);
        } else if (name.equals("FLYCAM_StrafeLeft")) {
            moveCamera(value, true);
        } else if (name.equals("FLYCAM_StrafeRight")) {
            moveCamera(-value, true);
        } else if (name.equals("FLYCAM_Rise")) {
            riseCamera(value);
        } else if (name.equals("FLYCAM_Lower")) {
            riseCamera(-value);
        }
    }
}
