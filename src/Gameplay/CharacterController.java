package Gameplay;

import Core.Main;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;

public class CharacterController extends AbstractControl implements ActionListener {

    private BetterCharacterControl player;
    private boolean left = false, right = false, up = false, down = false;
    private Vector3f walkDirection = new Vector3f();
    private Vector3f spawnLocation;
    private Vector3f playerLocation;
    private Node playerNode;
    private final Vector3f PLAYER_SPAWN_LOCATION = new Vector3f(-394f, -34f, -413f);
    private final Vector3f PLAYER_JUMP_SPEED = new Vector3f(0f, 12f, 0f);
    private final float PLAYER_MOVEMENT_SPEED = 35f;
    private final Vector3f SPAWN_VIEW_DIRECTION = new Vector3f(0.6f, 0f, 10.7f);
    private Camera cam;
    private InputManager iM;
    //Developer speed : un-comment and than comment above line.
    //private final float PLAYER_MOVEMENT_SPEED = 150f;

    public CharacterController(Camera c,
            float radius,
            float height,
            float mass,
            Vector3f gravity,
            InputManager iM) {
        this.iM = iM;
        //Setup physics and spatial for character node.
        cam = c;
        player = Environment.Scene.factory.CreatePlayer(radius,
                height,
                mass,
                PLAYER_JUMP_SPEED,
                gravity,
                this);
        for(int i = 0; i < Environment.Scene.factory.nodes.size(); i++)
        {
            if (Environment.Scene.factory.nodes.get(i).getName().equals("Player Node"))
            {
                playerNode = Environment.Scene.factory.nodes.get(i);
            }
        }
        SetupKeys();
        player.setEnabled(false);
    }

    public void SpawnCharacter() {
        //Enable and spawn the character.
        player.setEnabled(true);
        spawnLocation = new Vector3f(PLAYER_SPAWN_LOCATION);
        player.warp(spawnLocation);
        cam.lookAt(new Vector3f(SPAWN_VIEW_DIRECTION), Vector3f.ZERO);
    }

    public BetterCharacterControl GetPlayer() {
        return player;
    }

    private void SetupKeys() {
        //Map keys to the Input Manager and add listeners.
        iM.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        iM.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        iM.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        iM.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        iM.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        iM.addListener(this, "Left");
        iM.addListener(this, "Right");
        iM.addListener(this, "Up");
        iM.addListener(this, "Down");
        iM.addListener(this, "Jump");
    }

    public void onAction(String binding, boolean value, float tpf) {
        //Listen for left key and react.
        if (binding.equals("Left")) {
            if (value) {
                left = true;
            } else {
                left = false;
            }
            //Listen for right key and react.
        } else if (binding.equals("Right")) {
            if (value) {
                right = true;
            } else {
                right = false;
            }
            //Listen for up key and react.
        } else if (binding.equals("Up")) {
            if (value) {
                up = true;
            } else {
                up = false;
            }
            //Listen for down key and react.
        } else if (binding.equals("Down")) {
            if (value) {
                down = true;
            } else {
                down = false;
            }
            //Listen for jump key and react.
        } else if (binding.equals("Jump")) {
            player.jump();
        }
    }

    @Override
    protected void controlUpdate(float tpf) {
        //Camera movement and character movement.
        Vector3f camDir = cam.getDirection().clone().multLocal(0.6f);
        Vector3f camLeft = cam.getLeft().clone().multLocal(0.4f);
        camDir.set(cam.getDirection().multLocal(0.125f));
        camLeft.set(cam.getLeft().multLocal(0.08f));
        camDir.y = 0f;
        playerLocation = new Vector3f(0f, 0f, 0f);
        walkDirection.set(0f, 0f, 0f);
        if (left) {
            walkDirection.addLocal(camLeft);
        }
        if (right) {
            walkDirection.addLocal(camLeft.negate());
        }
        if (up) {
            walkDirection.addLocal(camDir);
        }
        if (down) {
            walkDirection.addLocal(camDir.negate());
        }
        playerLocation.set(playerNode.getLocalTranslation());
        playerLocation.y += 1.5;
        player.setWalkDirection(walkDirection.mult(PLAYER_MOVEMENT_SPEED));
        player.setViewDirection(camDir);
        cam.setLocation(playerLocation);
        cam.update();
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //nothing to do here
    }
}
