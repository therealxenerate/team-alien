package Core;

import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.SceneGraphVisitorAdapter;
import com.jme3.scene.Spatial;

public class TraverseGraph implements SceneGraphVisitor {

    public void visit(Spatial spatial) {
        DisplaySpatialInformation(spatial);
    }

    private void DisplaySpatialInformation(Spatial spatial) {
        System.out.println("\nSpatial Name: "
                + spatial.getName());
        if (spatial.getParent() != null) {
            System.out.println("Parent's Node: "
                    + spatial.getParent().getName());
        }
        System.out.println("Triangle count: "
                + spatial.getTriangleCount());
        System.out.println("Vertex count: "
                + spatial.getVertexCount());
    }

    public class TraverseGraphAdapter
            extends SceneGraphVisitorAdapter {

        @Override
        public void visit(Geometry geom) {
            System.out.println("Geometry Visited: " + geom);
        }
        
        @Override
        public void visit(Node node) {
            System.out.println("Node Visited: " + node);
        }
    }
}
