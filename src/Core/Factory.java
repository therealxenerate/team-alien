package Core;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioRenderer;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.input.InputManager;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.ImageBasedHeightMap;
import com.jme3.texture.Texture;
import java.util.ArrayList;
import java.util.List;

public class Factory {

    public ArrayList<Node> nodes = new ArrayList<Node>();
    private Camera c;
    private AssetManager aM;
    private InputManager iM;
    private Node rN;
    private AudioRenderer aR;
    private BulletAppState pH;

    public Factory(Camera c,
            AssetManager aM,
            InputManager iM,
            Node rN,
            AudioRenderer aR,
            BulletAppState pH) {
        this.c = c;
        this.aM = aM;
        this.iM = iM;
        this.rN = rN;
        this.aR = aR;
        this.pH = pH;
    }

    public void CreateTerrain() {
        TerrainQuad terrain;
        Material terrainMaterial;
        //Initialize terrain material.
        terrainMaterial = new Material(aM,
                "Common/MatDefs/Terrain/Terrain.j3md");

        //Set alpha map to material.
        terrainMaterial.setTexture("Alpha", aM.loadTexture(
                "Textures/Terrains/gaizervalleycm.png"));

        //Add grass to terrain.
        Texture grass = aM.loadTexture(
                "Textures/Terrain/splat/grass.jpg");
        grass.setWrap(Texture.WrapMode.Repeat);
        terrainMaterial.setTexture("Tex2", grass);
        terrainMaterial.setFloat("Tex1Scale", 64f);

        //Add dirt to terrain.
        Texture dirt = aM.loadTexture(
                "Textures/Terrain/splat/dirt.jpg");
        dirt.setWrap(Texture.WrapMode.Repeat);
        terrainMaterial.setTexture("Tex1", dirt);
        terrainMaterial.setFloat("Tex2Scale", 32f);

        //Implement HeightMap.
        AbstractHeightMap heightmap;
        Texture heightMapImage = aM.loadTexture(
                "Textures/Terrains/gaizervalleyhm.png");
        heightmap = new ImageBasedHeightMap(heightMapImage.getImage());
        heightmap.load();

        //Declare terrain quad and it's attributes.
        int patchSize = 65;
        terrain = new TerrainQuad("gaizer_valley_terrain", patchSize, 513, heightmap.getHeightMap());
        terrain.setMaterial(terrainMaterial);
        terrain.setLocalTranslation(0, -100, 0);
        terrain.setLocalScale(2f, 1f, 2f);
        rN.attachChild(terrain);

        //Camera controls for Level of Detail.
        List<Camera> cameras = new ArrayList<Camera>();
        cameras.add(c);
        TerrainLodControl control = new TerrainLodControl(terrain, cameras);
        terrain.addControl(control);

        //Add terrain to physics space and give it a rigidbodycontrol.
        terrain.addControl(new RigidBodyControl(0));
        pH.getPhysicsSpace().add(terrain);
    }

    public BetterCharacterControl CreatePlayer(float radius,
            float height,
            float mass,
            Vector3f jumpSpeed,
            Vector3f gravity,
            AbstractControl controller) {
        
        BetterCharacterControl player = new BetterCharacterControl(radius,
                height,
                mass);
        
        Geometry teaGeom = (Geometry) aM.loadModel("Models/Teapot/Teapot.obj");
        Node playerNode = new Node("Player Node");
        Material mat_tea = new Material(aM, "Common/MatDefs/Misc/ShowNormals.j3md");
        teaGeom.setMaterial(mat_tea);
        playerNode.attachChild(teaGeom);
        playerNode.addControl(player);
        player.setGravity(gravity);
        player.setJumpForce(jumpSpeed);
        playerNode.addControl(controller);
        pH.getPhysicsSpace().add(player);
        rN.attachChild(playerNode);
        
        nodes.add(playerNode);
        
        return player;
    }
}
