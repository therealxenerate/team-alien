package Core;

import Environment.Scene;
import com.jme3.app.FlyCamAppState;
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.system.AppSettings;

/**
 * BitBucket (Git) Information - Repository link:
 * https://therealxenerate@bitbucket.org/therealxenerate/team-alien.git
 */
public class Main extends SimpleApplication {

    private BulletAppState bulletAppState;
    private Scene currentScene;
    private final String BUILD_NAME = "Garaboor Alpha v0.01";

    public static void main(String[] args) {
        Main app = new Main();
        AppSettings settings = new AppSettings(true);
        settings.setAudioRenderer(AppSettings.LWJGL_OPENAL);
        settings.setTitle("Garaboor");
        settings.setResolution(800, 600);
        settings.setSettingsDialogImage("Images/Splash/app_garaboor.png");
        app.setSettings(settings);
        app.start();
    }

    @Override
    public void simpleInitApp() {

        //Completely remove the default FlyCam from our application
        stateManager.detach(stateManager.getState(FlyCamAppState.class));
        //Attach physics to the State Manager.
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        //Create & Initialize new Scene and set it to Gaizer Valley.
        Scene gaizerValley = new Scene(Environment.Scene.Setting.GaizerValley);
        gaizerValley.Init(cam,
                inputManager,
                assetManager,
                rootNode,
                audioRenderer,
                bulletAppState);
        SetCurrentScene(gaizerValley);
        AddWatermark();
        rootNode.breadthFirstTraversal(new TraverseGraph());
    }

    private void AddWatermark() {
        BitmapText watermark = new BitmapText(guiFont, false);
        watermark.setSize(guiFont.getCharSet().getRenderedSize());
        watermark.setColor(ColorRGBA.Red);
        watermark.setText(BUILD_NAME);
        float posX = settings.getWidth() - 190f;
        float posY = settings.getHeight() - 10f;
        float posZ = 0;
        watermark.setLocalTranslation(posX, posY, posZ);
        guiNode.attachChild(watermark);
    }

    public void SetCurrentScene(Scene scene) {
        this.currentScene = scene;
    }

    public BulletAppState GetBulletAppState() {
        return bulletAppState;
    }

    @Override
    public void simpleUpdate(float tpf) {
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
}
